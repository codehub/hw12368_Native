/*
 * Copyright (c) [2019] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * encTurbo is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 *
 *     http://license.coscl.org.cn/MulanPSL
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

#ifndef VA_ENCODE_COMMON
#define VA_ENCODE_COMMON

#include <stdbool.h>

/**
 * 返回值定义
 */
typedef enum {
    ENC_STATUS_SUCCESS = 0x00000000,
    ENC_STATUS_ERROR_INVALID_PARAMETER = 0x00000001, // 参数传输错误
    ENC_STATUS_ERROR_OPEN_DISPLAY_DEVICE = 0x00000002, // 驱动设备节点打开错误
    ENC_STATUS_ERROR_DISPLAY_HANDLE = 0x00000003, // display设备错误
    ENC_STATUS_ERROR_MALLOC = 0x00000004, // 申请内存错误
    ENC_STATUS_ERROR_CONTEXT_HANDLE = 0x00000005, // 上下文指针错误
    ENC_STATUS_ERROR_CONFIG = 0x00000006, // va 配置错误
    ENC_STATUS_ERROR_VA_STATUS = 0x00000007, // va 返回值错误
    ENC_STATUS_ERROR_VA_SYNC_STATUS = 0x00000008, // va 编码错误
} ENCStatus;

/**
 * Input source type 
 */
typedef enum {
    SOURCE_RAW_BO, // the source data is GPU bo handle and the raw data is RGB format.
    SOURCE_RAW_YUV_FILE, // the source data is CPU file handle and the raw data is YUV format.
} SourcRawType;

/**
 * Rate control 模式，支持CBR/VBR
 */
typedef enum {
    ENC_RC_CBR, // 恒定比特率。视觉质量稳定性：不稳定；即时输出码率：恒定；输出文件大小：可控
    ENC_RC_VBR, // 动态比特率。视觉质量稳定性：稳定；即时输出码率：变化；输出文件大小：不可控
} EncRateControl;

/**
 * 画质级别
 */
typedef enum {
    ENC_PROFILE_IDC_BASELINE = 66, // 基本画质。支持I/P 帧，只支持无交错（Progressive）和CAVLC
    ENC_PROFILE_IDC_MAIN = 77, // 主流画质。提供I/P/帧，支持无交错（Progressive），也支持CAVLC 和CABAC 的支持
} EncProfileIdc;

/**
 * 输出数据类型
 */
typedef enum {
    OUTPUT_H264, // 输出H264数据
    OUTPUT_YUV, // 输出YUV数据
} OutputDataType;

/**
 * GPU驱动路径
 */
typedef enum {
    DEV_DRI_RENDERD128 = 128, // "/dev/dri/renderD128"
    DEV_DRI_RENDERD129 = 129, // "/dev/dri/renderD129"
    DEV_DRI_RENDERD130 = 130, // "/dev/dri/renderD130"
    DEV_DRI_RENDERD131 = 131, // "/dev/dri/renderD131"
    DEV_DRI_RENDERD132 = 132, // "/dev/dri/renderD132"
    DEV_DRI_OTHER, // NULL
} DRMDevicePath;

/**
 * Input soure type and file path 
 */
typedef struct {
    SourcRawType source_raw_type; // if is SOURCE_RAW_YUV_FILE, sourceDataBuffer should be local YUV data
    OutputDataType output_data_type;
    int reserved;
} avcencode_input_s;

/**
 * Input config value 
 */
typedef struct {
    int width_input; // 输入数据宽度
    int height_input; // 输入数据高度
    int width_out; // 输出数据宽度
    int height_out; // 输入数据高度
    EncRateControl rate_control; // 码率控制模式
    int bit_rate; // 比特率
    int frame_rate; // 帧率
    int gop_size; // 图像组
    EncProfileIdc profile_idc;
    DRMDevicePath drm_device_path;
} avcencode_config_s;

/**
 * 输出数据返回结构体 
 */
typedef struct {
    void *coded_mem; // 数据指针
    int coded_mem_size; // 数据大小
} avcenc_codedbuf_s;
#endif
